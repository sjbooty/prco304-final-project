﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoScript : MonoBehaviour {

	bool isPaused = false;
	void Update () {
		if (Input.GetKeyDown ("space"))
		if (isPaused) {
			isPaused = false;
			Time.timeScale = 1.0f;
		} else {
			isPaused = true;
			Time.timeScale = 0.2f;
			}
	}

}
