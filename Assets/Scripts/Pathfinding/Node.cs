﻿using UnityEngine;
using System.Collections;

public class Node : IHeapItem<Node> { //now implements heap interface

	public bool isValidNode;				//obtacles will hold a different state for walable or unwalkable
	public Vector3 realWorldPosition;  //holds the real position of the node in the unity scene
	public int gridValueX;
	public int gridValueY;

	public int gCost;  //distance from starting node
	public int hCost;  //distance to end node

	public Node parent;  //Used to set the new fcost
	int heapIndex;


	public Node(bool _walkable, Vector3 _realWorldPos, int _gridValueX, int _gridValueY) {  //Passing grid variables to find its neighbors
		isValidNode = _walkable;
		realWorldPosition = _realWorldPos;
		gridValueX = _gridValueX;
		gridValueY = _gridValueY;
	}

	public int fCost {  //returns fcost
		get {
			return gCost + hCost;
		}
	}

	public int heapItemIndex { //find heap index and set heap index

		get{
			return heapIndex;
		}
		set{
			heapIndex = value;
		}
}

	public int CompareTo(Node nodeToCompare) {  //compare a nodes fcost and hcost to another, gcost comparisons are not required as the distance to the end node will be used first.
		int compare = fCost.CompareTo (nodeToCompare.fCost);
		if (compare == 0) {
			compare = hCost.CompareTo (nodeToCompare.hCost);
		}
		return -compare;
	}
}
