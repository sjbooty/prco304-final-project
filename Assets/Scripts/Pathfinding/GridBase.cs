﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridBase : MonoBehaviour {


	public bool displayGridGizmoz;
	public LayerMask unwalkableLayerMask;   //Which layer will be the obstacles
	public Transform target;				//The target to be followed
	public Vector2 gridXYSize;				//The real world size of the grid
	public float nodeSize; 					//The real world size of each node
	Node[,] grid;

	float nodeDiameter;
	int gridSizeX, gridSizeY;

	void Awake() {
		nodeDiameter = nodeSize * 2;  //Multiplied out as node size needs to be larger for performance
		gridSizeX = Mathf.RoundToInt(gridXYSize.x / nodeDiameter);  //Find the number of nodes in the x axis
		gridSizeY = Mathf.RoundToInt(gridXYSize.y / nodeDiameter); 	//Find the number of nodes in the y axis
		CreateGrid ();    //Make the grid
	}



	public int GetGridMaxSize {
		get {
			return gridSizeX * gridSizeY;
		}
	}


	public void CreateGrid () {
		grid = new Node[gridSizeX, gridSizeY];  //Create a 2d array of nodes equal to the found values
		Vector3 worldBottomLeft = transform.position - Vector3.right * gridXYSize.x / 2 - Vector3.forward * gridXYSize.y / 2; //Find bottom left point on the grid by taking half of the x and y values away



		for (int x = 0; x < gridSizeX; x++) {  //Check through all of the nodes in the 2D array and check which nodes are unwalkable
			for (int y = 0; y < gridSizeY; y++) {

				Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeSize)
									+ Vector3.forward * (y * nodeDiameter + nodeSize);  //Create a world point reference for each node in the array

				bool walkable = !(Physics.CheckSphere (worldPoint, nodeSize, unwalkableLayerMask));  //make a bool and return true if there are no collisions in the sphere overlap
				grid [x, y] = new Node (walkable, worldPoint, x, y);  //Create a new node and pass in its walkable state and its real world point


			}
		}
	}

	public List<Node> GetNeighbors(Node node) {  //Find a list of all neighbor nodes, normally 8 but could be less when current node is an edge node
		List<Node> neighbors = new List<Node>();  //List of nodes for the neighbors

		for (int x = -1; x <= 1; x++) {  //loop through each 3 nodes in a horizontal and vertical pattern, ignore x and y value of 0 as this is the current node
			for (int y = -1; y <= 1; y++) {
				if (x == 0 && y == 0) {
					continue;  //skip
				}

					int checkX = node.gridValueX + x;
					int checkY = node.gridValueY + y;

					if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY) {  //Check if the node being checked is still within the grid array toal size
						neighbors.Add(grid[checkX, checkY]);

				}
			}
		}
		return neighbors;  //Return all valid neighbors
	}

	public Node NodeFromWorldPoint(Vector3 realWorldPosition) {  //required to find the node the player is standing in
		float percentX = (realWorldPosition.x + gridXYSize.x / 2) / gridXYSize.x; //Find the x percentage point along the grid from the world position
		float percentY = (realWorldPosition.z + gridXYSize.y / 2) / gridXYSize.y; ////Find the y percentage point along the grid from the world position (2D array so z not y)
		percentX = Mathf.Clamp01 (percentX);  //Convert the values to a value between 0 and 1 (%)
		percentY = Mathf.Clamp01 (percentY);

		int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);  //Find the node values for x and y from the zero based array
		int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
		return grid [x, y];										//Return the node values for x and y
	}


	void OnDrawGizmos() {
		Gizmos.DrawWireCube (transform.position, new Vector3 (gridXYSize.x, 1, gridXYSize.y));

		if (grid != null && displayGridGizmoz) {

			Node playerNode = NodeFromWorldPoint (target.position);
				
			foreach (Node n in grid) {
				Gizmos.color = (n.isValidNode) ? Color.grey : Color.red;
				Gizmos.DrawCube (n.realWorldPosition, new Vector3((nodeDiameter - 0.1f), 0.3f, (nodeDiameter - 0.1f)));


				}
			}
		}
	}


