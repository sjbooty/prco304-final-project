﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;

public class RequestManager : MonoBehaviour {

	Queue<PathEnd> allRequestsQueue = new Queue<PathEnd> ();  //queue of all requests results
	static RequestManager thisReference;  //Thread instance
	AStar aStar;


	void Awake() {
		thisReference = this;
		aStar = GetComponent<AStar> ();
	}
		
	void Update() {
		if (allRequestsQueue.Count > 0) {
			int itemsFromQueue = allRequestsQueue.Count;
			lock (allRequestsQueue) {
				for (int i = 0; i < itemsFromQueue; i++) {
					PathEnd end = allRequestsQueue.Dequeue ();
					end.callbackMain (end.plotPoints, end.hasBeenFound);
				}
			}
		}
	}


	public static void NewPath(NewPath request) {  //Start find plot points in a new thread
		ThreadStart thread = delegate {
			thisReference.aStar.FindNodePath (request, thisReference.CalculatedPath);  //Method to call on the new thread
		};
		thread.Invoke ();
	}
		


	public void CalculatedPath(PathEnd result) { //Return to main thread and make the callback using queue
		lock (allRequestsQueue) {
			allRequestsQueue.Enqueue (result);
		}
	}



}

public struct PathEnd {  //Result of all paths struct
	public Vector3[] plotPoints;
	public bool hasBeenFound;
	public Action<Vector3[], bool> callbackMain;

	public PathEnd (Vector3[] points, bool foundResult, Action<Vector3[], bool> callback) {
		this.plotPoints = points;
		this.hasBeenFound = foundResult;
		this.callbackMain = callback;
	}
}

public struct NewPath {  //Each path request holds its start and end node as well as a callback
	public Vector3 pathStart;
	public Vector3 pathFinish;
	public Action<Vector3[], bool> callbackMain;

	public NewPath(Vector3 _pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback) {
		pathStart = _pathStart;
		pathFinish = pathEnd;
		callbackMain = callback;
	}
}
