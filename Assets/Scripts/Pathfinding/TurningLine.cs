﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct TurningLine {
	const float verticalGradient = 1000000.0f;

	float lineGradient;
	float lineYIntercept;
	Vector2 firstLinePoint;
	Vector2 scecondLinePoint;

	float perpendicularLineGradient;

	bool agentSide;

	public TurningLine(Vector2 linePoint, Vector2 perpendicularLinePoint) {  
		float dx = linePoint.x - perpendicularLinePoint.x;  
		float dy = linePoint.y - perpendicularLinePoint.y;

		if (dx == 0) {  //if gradient is 0 stop calculation and have a very large gradient to avoid divide by zero error
			perpendicularLineGradient = verticalGradient;
		} else {
			perpendicularLineGradient = dy / dx; //Find gradient of perpendicular line
		}

		if (perpendicularLineGradient == 0) {  //Perpendicular line gradient multipled by line graient is equal to minus 1, reverse calculate unless its zero
			lineGradient = verticalGradient;
		} else {
			lineGradient = -1 / perpendicularLineGradient;  
		}

		lineYIntercept = linePoint.y - lineGradient * linePoint.x; 	//Rearanged y = mx + c to find C or the point at which to start turning
		firstLinePoint = linePoint;
		scecondLinePoint = linePoint + new Vector2 (1, lineGradient);

		agentSide = false;
		agentSide = GetAgentSide (perpendicularLinePoint);   //Assign a bool vlaue to which side of the point the agent is on
	}

	bool GetAgentSide(Vector2 p) {  //Find the side the agent is of a plot point to calculate turn point
		return (p.x - firstLinePoint.x) * (scecondLinePoint.y - firstLinePoint.y) > (p.y - firstLinePoint.y) * (scecondLinePoint.x - firstLinePoint.x);
	}

	public bool HasCrossedLine(Vector2 p) { //Reverse the getAgentsSide to get new line crossed value
		return GetAgentSide (p) != agentSide;
	}

	public float DistanceFromPoint(Vector2 p) {
		float yInterceptPerpendicular = p.y - perpendicularLineGradient * p.x;
		float intersectX = (yInterceptPerpendicular - lineYIntercept) / (lineGradient - perpendicularLineGradient);
		float intersectY = lineGradient * intersectX + lineYIntercept;
		return Vector2.Distance (p, new Vector2 (intersectX, intersectY));
	}

	public void DrawWithGizmos(float length) {
		Vector3 lineDirection = new Vector3 (1, 0, lineGradient).normalized;
		Vector3 lineCenter = new Vector3 (firstLinePoint.x, 1 * 10.95f, firstLinePoint.y) + Vector3.up;
		Gizmos.DrawLine (lineCenter - lineDirection * length / 2f, lineCenter + lineDirection * length / 2f);
	}
}
