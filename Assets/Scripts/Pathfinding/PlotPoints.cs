﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlotPoints {
	
	public readonly Vector3[] transformLookPoints;
	public readonly TurningLine[] turnPointsLength;
	public readonly int finishLineIndexpoint;
	public readonly int slowIndexPoint;

	public PlotPoints(Vector3[] points, Vector3 start, float turnDistance, float slowingDistance) {
		transformLookPoints = points;   //Each turning point on the line
		turnPointsLength = new TurningLine[transformLookPoints.Length];
		finishLineIndexpoint = turnPointsLength.Length - 1;   //zero based array so minus one point

		Vector2 lastPoint = V3ToV2 (start);
		for (int i = 0; i < transformLookPoints.Length; i++) {
			Vector2 currentPoint = V3ToV2 (transformLookPoints [i]);
			Vector2 dirToCurrentPoint = (currentPoint - lastPoint).normalized;
			Vector2 turnBoundaryPoint = (i == finishLineIndexpoint)?currentPoint : currentPoint - dirToCurrentPoint * turnDistance;  //If the finish line point is selected, no longer need to minus turndistance as there is no point to turn to
			turnPointsLength [i] = new TurningLine (turnBoundaryPoint, lastPoint - dirToCurrentPoint * turnDistance); //feed two points to calculate the Gradient and give TurningLine points as distance anway from the line
			lastPoint = turnBoundaryPoint;
		}

		float dstFromEndPoint = 0;
		for (int i = transformLookPoints.Length - 1; i > 0; i--) {
			dstFromEndPoint += Vector3.Distance (transformLookPoints [i], transformLookPoints [i - 1]);  //Make a distance from end node, if the end node distance is less than desired float, slow the agent down
			if (dstFromEndPoint > slowingDistance) {
				slowIndexPoint = i;
				break;
			}
		}
	}

	Vector2 V3ToV2(Vector3 v3) { //Convert 3D point to 2D for grid
		return new Vector2 (v3.x, v3.z);
	}

	public void DrawWithGizmos() {
		Gizmos.color = Color.blue;
		foreach (Vector3 p in transformLookPoints) {
			Gizmos.DrawCube (p + Vector3.up, Vector3.one);
		}

		Gizmos.color = Color.green;
		foreach (TurningLine line in turnPointsLength) {
			line.DrawWithGizmos (10);
		}

	}
}
