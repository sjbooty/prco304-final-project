﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AStar : MonoBehaviour {
	//int counter = 0;

	GridBase grid;

	void Awake() {
		grid = GetComponent<GridBase> ();   //Find the grid on this gameobject
	}
		



	public void FindNodePath(NewPath request, Action<PathEnd> callback) {
		Vector3[] nodeWaypoints = new Vector3[0];
		bool pathSuccess = false;


		Node startNode = grid.NodeFromWorldPoint (request.pathStart);   //The passed variable for the starting node
		Node endNode = grid.NodeFromWorldPoint (request.pathFinish);	//the passed variable for the end node

		if (startNode.isValidNode && endNode.isValidNode) {
			HeapOptimisation<Node> openNodeSet = new HeapOptimisation<Node> (grid.GetGridMaxSize);   //Add all nodes from the grid to a heap
			HashSet<Node> closedNodeSet = new HashSet<Node> ();	      //Create an empty set which will eventually contain the closed nodes.
			openNodeSet.AddItem (startNode);

			while (openNodeSet.CountTotalHeapItems > 0) {
				//counter++;
				Node selectedNode = openNodeSet.RemoveFirstItem ();   //Set the first node, by removing from heap
		
				closedNodeSet.Add (selectedNode);

				if (selectedNode == endNode) {  //If the current node is the target then break the loop and set the path success bool to true
					pathSuccess = true;

					break;
				}

				foreach (Node neighbor in grid.GetNeighbors(selectedNode)) {    //Check each neighbor of the current node
					if (!neighbor.isValidNode || closedNodeSet.Contains (neighbor)) {  //If a neighbor node is unwalable or it has been removed form the open set, skip this iteration
						continue;
					}

					int newMovementCostToNeighbor = selectedNode.gCost + GetDistanceAB (selectedNode, neighbor);  //Check if a new path costs less than a new path
					if (newMovementCostToNeighbor < neighbor.gCost || !openNodeSet.ContainsHeapItem (neighbor)) {  //if the cost is less, set the f cost by setting the gcost and hcost values
						neighbor.gCost = newMovementCostToNeighbor; //set to distance from start node
						neighbor.hCost = GetDistanceAB (neighbor, endNode); //set to distance to end node
						neighbor.parent = selectedNode;

						if (!openNodeSet.ContainsHeapItem (neighbor)) {
							openNodeSet.AddItem (neighbor);  //Add the neighbor to the heap

						} else {
							openNodeSet.UpdateHeapItem (neighbor);  //update the neighbor item in the heap
						}
					}
				}
			}
		}

		if (pathSuccess) {
			nodeWaypoints = CalculatePath(startNode, endNode);
			pathSuccess = nodeWaypoints.Length > 0;
			//Debug.Log (counter);
		}

		callback(new PathEnd(nodeWaypoints, pathSuccess,request.callbackMain));
	
	
	}

	Vector3[] CalculatePath(Node startNode, Node endNode) {  //calculate the path using the closed set nodes parents back from the end node to the start node
		List<Node> path = new List<Node>();
		Node currentNode = endNode;

		while (currentNode != startNode) {
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}
		Vector3[] waypoints = SimplifyPath (path);
		Array.Reverse (waypoints);
		return waypoints;

	}

	Vector3[] SimplifyPath(List<Node> path) {  //Calculate the path with waypoints that are only changed if the direction changes
		List<Vector3> waypoints = new List<Vector3> ();
		Vector2 directionOld = Vector2.zero;

		for (int i = 1; i < path.Count; i++) {
			Vector2 directionNew = new Vector2 (path [i - 1].gridValueX - path[i].gridValueX, path[i - 1].gridValueY - path [i].gridValueY);
			if (directionNew != directionOld) {
				waypoints.Add (path [i].realWorldPosition);
			}
			directionOld = directionNew;
			}
		return waypoints.ToArray();

		}


	int GetDistanceAB(Node nodeA, Node nodeB) {    //Take in two nodes and calculate the distance from the start node to the end node. (14y + 10(x-y))  14 being diagonal cost and 10 being horizontal cost or vertical cost
		int dstX = Mathf.Abs (nodeA.gridValueX - nodeB.gridValueX);
		int dstY = Mathf.Abs (nodeA.gridValueY - nodeB.gridValueY);

		if (dstX > dstY) {
			return 14 * dstY + 10 * (dstX - dstY);
		} else {
			return 14 * dstX + 10 * (dstY - dstX);
		}
	}
}
