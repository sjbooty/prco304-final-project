﻿using UnityEngine;
using System.Collections;
using System;

public class HeapOptimisation<T> where T : IHeapItem<T>{ //Takes a type instead of a class so that it is generic


	T[] itemsArray;
	int currentItemCount;



	public HeapOptimisation(int maxHeapSize) {
		itemsArray = new T[maxHeapSize];  //make the heap size equal to the total number of items
	}

	public void AddItem(T item) {  //add an item to the heap and correct its position
		item.heapItemIndex = currentItemCount;
		itemsArray [currentItemCount] = item;
		SortUpHeapItems (item);  //correct item position in heap
		currentItemCount++;  //add an item to the count
	}

	public T RemoveFirstItem() {  //Remove the first item in the heap
		T firstItem = itemsArray [0];
		currentItemCount--;
		itemsArray [0] = itemsArray [currentItemCount];
		itemsArray [0].heapItemIndex = 0;
		SortDownHeapItems (itemsArray [0]);
		return firstItem;
	}

	public void UpdateHeapItem(T item) {  //Update heap with the new item after finding item in open set
		SortUpHeapItems (item);

	}


	public int CountTotalHeapItems {  //returns the number of items in the current heap
		get { return currentItemCount;}
	}

	public bool ContainsHeapItem(T item) {  //Check if the heap contains requested item
		return Equals (itemsArray[item.heapItemIndex], item);
	}

	void SortDownHeapItems(T item) {
		while (true) {
			int childIndexOne = item.heapItemIndex * 2 + 1;  //find the first child
			int childIndexTwo = item.heapItemIndex * 2 + 2;	//find the second child
			int swapIndex = 0;

			if (childIndexOne < currentItemCount) { //find the child (if it exists) and swap their itemindex based on which has the lower fcost
				swapIndex = childIndexOne;

				if (childIndexTwo < currentItemCount) {
					if(itemsArray[childIndexOne].CompareTo(itemsArray[childIndexTwo]) < 0) {
						swapIndex = childIndexTwo;
					}
				}

						if(item.CompareTo(itemsArray[swapIndex]) < 0) {
							SwapHeapItems(item,itemsArray[swapIndex]);
						} else {
							return;
						}
			}
						else{  //if no children, correct position assumed
							return;
						}
		}
	}

	void SortUpHeapItems(T item) {  
		int parentIndex = ((item.heapItemIndex - 1) / 2); //find the items parent (n-1)/2

		while (true) {
			T parentItem = itemsArray [parentIndex];
			if (item.CompareTo (parentItem) > 0) { //compare the item to its parent, if the item has a lower fcost, swap with its parent
				SwapHeapItems (item, parentItem);
			} else {  //if it had a higher fcost, break the loop
				break;
			}
		}
	}

	void SwapHeapItems(T itemA, T itemB) {  //simple method to switch two items
		itemsArray [itemA.heapItemIndex] = itemB;
		itemsArray [itemB.heapItemIndex] = itemA;

		int itemAIndex = itemA.heapItemIndex;
		itemA.heapItemIndex = itemB.heapItemIndex;
		itemB.heapItemIndex = itemAIndex;
	}

}

public interface IHeapItem<T> : IComparable<T> { //Required to gurantee that the items are comparable
	int heapItemIndex {
		get;
		set;
	}
}
