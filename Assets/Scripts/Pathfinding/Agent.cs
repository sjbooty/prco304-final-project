﻿using UnityEngine;
using System.Collections;

public class Agent : MonoBehaviour {


	const float minPathChangeTime = .2f;
	const float movementPathDistanceUpdate = .5f;

	public Transform targetPosition;
	public float agentSpeed = 5;
	public float agentTurnSpeed = 5;
	public float turnDistance = 5;
	public float stoppingDistance = 5;
	public bool drawGizmos = false;

	PlotPoints plotPoints;

	void Start() {
			targetPosition = GameObject.FindWithTag ("Player").transform;
			StartCoroutine (UpdateNewPath ());
	}

	public void OnPathFound(Vector3[] pathWaypoints, bool pathSuccessful) {  //If a path has been found calculate the plot points to move along and execute the agents path
		if (pathSuccessful && targetPosition != null) {
			plotPoints = new PlotPoints (pathWaypoints, transform.position, turnDistance, stoppingDistance);
			StopCoroutine ("AgentFollowPath");
			StartCoroutine ("AgentFollowPath");
		}
	}

	IEnumerator UpdateNewPath() { //If the targets positions has moved over a threshhold re calculate the path to the target by requesting a new path

		if (Time.timeSinceLevelLoad < .3f) {
			yield return new WaitForSeconds (.3f);
		}
		RequestManager.NewPath (new NewPath(transform.position, targetPosition.position, OnPathFound));

		float sqrMoveCheck = movementPathDistanceUpdate * movementPathDistanceUpdate;
		Vector3 oldTargetPosition = targetPosition.position;

		while (true) {
			if(targetPosition != null) {
			yield return new WaitForSeconds (minPathChangeTime);
			if ((targetPosition.position - oldTargetPosition).sqrMagnitude > sqrMoveCheck) {
					RequestManager.NewPath (new NewPath(transform.position, targetPosition.position, OnPathFound));
				oldTargetPosition = targetPosition.position;
			}
		}
	}
	}

	IEnumerator AgentFollowPath() {  //Execture a number of lookrotatiosn and moves to follow it point on a pathpoint array in order

		bool currentlyOnPath = true;
		int pathIndex = 0;
		transform.LookAt (plotPoints.transformLookPoints [0]);

		float speedPercent = 1;

		while (currentlyOnPath) {
			Vector2 pos2D = new Vector2 (transform.position.x, transform.position.z);  //Iterate through the paths index and if the current path node is equal to the target node, break out of the coroutine
			while (plotPoints.turnPointsLength [pathIndex].HasCrossedLine (pos2D)) {
				if (pathIndex == plotPoints.finishLineIndexpoint) {
					currentlyOnPath = false;
					break;
				} else {
					pathIndex++;
				}
			}

			if (currentlyOnPath) {

				if (pathIndex >= plotPoints.slowIndexPoint && stoppingDistance > 0) {  //If the current point is the slowing point of the path, clamp the speed relative to its distance to target
					speedPercent = Mathf.Clamp01 (plotPoints.turnPointsLength [plotPoints.finishLineIndexpoint].DistanceFromPoint (pos2D) / stoppingDistance);
					if (speedPercent < 0.01f) {
						currentlyOnPath = false;  //If the speed falls below 0.01 cancel the path and be ready to calculate new path
					}
				}

				Quaternion targetRotation = Quaternion.LookRotation (plotPoints.transformLookPoints [pathIndex] - transform.position);  //Execute path
				transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, Time.deltaTime * agentTurnSpeed);
				transform.Translate (Vector3.forward * Time.deltaTime * agentSpeed * speedPercent, Space.Self);
			}

			yield return null;

		}
	}

	public void OnDrawGizmos() {
		if (plotPoints != null && drawGizmos) {
			plotPoints.DrawWithGizmos ();
		}
	}
}

