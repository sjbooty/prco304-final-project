﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
	public Text scoreText;
	public Text highScoreText;
	private int score = 0;
	private int highscore = 0;

	public void Start() {  //Find player prefs and save the int to local variable
		scoreText.text = score + "";
		highscore = PlayerPrefs.GetInt ("highscore"); 
		highScoreText.text = highscore + "";
	}
	public void FixedUpdate() { //On a fixed update loop continuously give the player more score
		score++;
		scoreText.text = score + "";
	}
		
	public void EnemyDied() {   //When an enemy dies, add 1000 points to the players score.
		score += 1000;
		scoreText.text = score + "";
	}

	public void PlayerDied() {  //Set a new highscore if the new score is higher than the old highscore
		if (score > highscore) {
			highscore = score;
			PlayerPrefs.SetInt ("highscore", highscore);
			highScoreText.text = highscore + "";
		}
	}
		


}
