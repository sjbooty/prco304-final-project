﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomiseSeed : MonoBehaviour {

	public MapGeneration map;
	public float delay = 1.0f;
	public void Start() {
		StartCoroutine (GenMap());
	}

	IEnumerator GenMap() {
		while (true) {
			yield return new WaitForSeconds (delay);
			GenerateMap ();
		}
	}

	void GenerateMap() {
		map.GenerateMap();
	}
	}

