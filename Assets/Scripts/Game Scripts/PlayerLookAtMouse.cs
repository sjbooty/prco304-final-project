﻿using UnityEngine;
using System.Collections;

public class PlayerLookAtMouse : MonoBehaviour {

    public float lookSpeed = 4.0f;

    void FixedUpdate()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);  //Take a ray from the camera to the mouse position

        float hitDist = 0.0f;

        if(playerPlane.Raycast(ray, out hitDist))
        {
            Vector3 targetPoint = ray.GetPoint(hitDist);

            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
			transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, lookSpeed * Time.deltaTime); //Rotate the player transform to face the screentoworld position
        }

    }
}
