﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateObject : MonoBehaviour {
	public delegate void DeathAction();
	public static event DeathAction OnPlayerShoot;
	public string shootButton; //Keeping this public allows it to be reused easily
	public Transform turret;
	public Rigidbody projectile;
	public float speed = 5.0f;
	public float shootTimer = 100.0f;
	private float counter = 0.0f;
	public AudioClip[] laserSounds;
	private AudioSource audio;
	Rigidbody rigidBody;
	Vector3 SpawnVector;

	void Start() {
		rigidBody = GetComponent<Rigidbody> ();
		audio = GetComponent<AudioSource> ();
	}
		
	//Counter sets the amount of time before the player is able to shoot another bullet, shoot timer is the amount of time remaining
	void FixedUpdate () {
		counter++;
		if (Input.GetButton(shootButton)) { //Retunrs true when a user presses a specific button
			if (counter >= shootTimer) {
				if (OnPlayerShoot != null) {
					OnPlayerShoot (); //Sends a message out theat the player has shot, retuns null if there are no listenewrs to avoid error
				}
					
				SpawnVector = new Vector3 (turret.position.x, turret.position.y, turret.position.z);
				Rigidbody instantiatedProjectile = Instantiate (projectile, transform.position, transform.rotation) as Rigidbody; //Spawn the object from the given transform position
				instantiatedProjectile.velocity = turret.TransformDirection (Vector3.forward * speed);

				Vector3 v = instantiatedProjectile.GetRelativePointVelocity (Vector3.forward * speed); //Set the speed of the object
				//v += rigidBody.velocity;
				//instantiatedProjectile.velocity = v;

				audio.clip = laserSounds [Random.Range (0, laserSounds.Length)]; //play a Random laserSounds from an array
				audio.Play ();
				counter = 0.0f;
			}
		}
	}
}
