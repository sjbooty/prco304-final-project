﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookDirectionRightStick : MonoBehaviour {

	float heading = 0.0f;
	public float lookSpeed = 10.0f;

	void Update () {
		heading = Mathf.Atan2((Input.GetAxis("RightH")), (Input.GetAxis("RightV"))); //Takes the direction of the right anaolog stick and converts it to the players bearing

		if (heading != 0) { //Only update when the player moves the right stick to the direction of the stick
			transform.rotation = Quaternion.Euler (0, heading * Mathf.Rad2Deg, 0);
			Quaternion targetRotation = Quaternion.LookRotation(Vector3.zero - transform.position);
			transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, lookSpeed * Time.deltaTime);
		}
	}
}
