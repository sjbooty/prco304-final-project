﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Countdown321 : MonoBehaviour {

	private string countdownText = "";
	private bool isCountdownVisible = false;
	public GUIStyle box;
	public AudioClip[] soundsToPlay;
	private AudioSource audio;

	void Start() {
		audio = GetComponent<AudioSource> ();
		StartCoroutine (Ready ());
	}

	//A series of wait times and play sound effects to make the countdown loop
	IEnumerator Ready() {
		isCountdownVisible = true; //Make the countdown visible

		yield return new WaitForSeconds (1.0f);
		audio.clip = soundsToPlay [0]; //reset each sound to its position in the array
		audio.Play ();
		countdownText = "3...";
		yield return new WaitForSeconds (1.0f);
		audio.clip = soundsToPlay [1];
		audio.Play ();
		countdownText = "2...";
		yield return new WaitForSeconds (1.0f);
		audio.clip = soundsToPlay [2];
		audio.Play ();
		countdownText = "1...";
		yield return new WaitForSeconds (1.0f);
		audio.clip = soundsToPlay [3];
		audio.Play ();
		isCountdownVisible = false;
	}

	void OnGUI() {
		GUI.Box (new Rect (Screen.width / 2, Screen.height/2 - Screen.height/100*40, 100, 50), "Next Level In...", box); //stays the same throughout

		GUI.Box (new Rect (Screen.width / 2, Screen.height/2 - Screen.height/100*30, 100, 50), countdownText, box); //Number variable creates the location on the screen depending on the users screen width
	}
}
