﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public GameObject menuGroup;
	public GameObject optionsGroup;
	public GameObject tutorialGroup;
	public GameObject tutorialObjectsGroup;

	public Toggle[] resolutionButtons;
	public int[] screenSizes;
	int currentScreenIndex;


	public void StartGame() {
		SceneManager.LoadScene (1);
	}

	public void QuitGame() {
		Application.Quit ();
	}

	public void SwapToOptions() {
		menuGroup.SetActive (false);
		optionsGroup.SetActive (true);
	}

	public void SwapToMainMenu() {
		menuGroup.SetActive (true);
		optionsGroup.SetActive (false);
		tutorialGroup.SetActive (false);
		tutorialObjectsGroup.SetActive (false);
	}

	public void SwapToTutorial() {
		menuGroup.SetActive (false);
		tutorialGroup.SetActive (true);
		tutorialObjectsGroup.SetActive (true);
	}

	public void SetResolution(int index) { //Set resolution using screen width applied to aspect ratio
		if (resolutionButtons [index].isOn) {
			currentScreenIndex = index;
			float aspectRatio = 16 / 9f;
			Screen.SetResolution(screenSizes[index], (int)(screenSizes[index]/aspectRatio), false);
		}
	}

	public void SetFullscreen(bool isFullscreen) {
		Screen.fullScreen = !Screen.fullScreen;
	}


}
