﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour { 
	private float playerMaxHealth = 0.0f;
	private float playerCurrentHealth = 0.0f;
	public Player playerScript;
	public Transform bar;

	//Take the barks local scale and make its length equaul to "100%"
	void Start () {
		playerMaxHealth = playerScript.startingHealth;
		playerCurrentHealth = playerMaxHealth;
		bar.localScale = new Vector3 (playerCurrentHealth / playerMaxHealth, bar.localScale.y, bar.localScale.z);
	}
	
	//Update its percentage as you lose health
	void Update () {
		playerCurrentHealth = playerScript.health;
		bar.localScale = new Vector3 (playerCurrentHealth / playerMaxHealth, bar.localScale.y, bar.localScale.z);
	}
}
