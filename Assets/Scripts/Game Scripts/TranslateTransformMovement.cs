﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateTransformMovement : MonoBehaviour {
	public int movementspeed = 100;
	void Start () {

	}
		
	void Update () { //Standard application for wasd movement, multiplied by public variable to make it changeable
		if (Input.GetKey (KeyCode.A)) {
			transform.Translate (Vector3.left * movementspeed * Time.deltaTime); 
		}
		if(Input.GetKey (KeyCode.D)) {
			transform.Translate (Vector3. right *   movementspeed * Time.deltaTime);
		}
		if(Input.GetKey (KeyCode.W)) {
			transform.Translate (Vector3. forward *   movementspeed * Time.deltaTime);
		}
		if(Input.GetKey (KeyCode.S)) {
			transform.Translate (Vector3. back *   movementspeed * Time.deltaTime);
		}

	}
}
