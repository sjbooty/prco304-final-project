﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEnemyOnContact : MonoBehaviour {
	public LayerMask collisionMask;
	public float damage = 1.0f;
	public float lifetime = 3.0f;
	public GameObject alienModel;

	//Destroy this object after "lifetime" amount of time
	void Awake() {
		Destroy (gameObject, lifetime);
	}

	//When this gameobjects collider collides with another gameobject this method is called
	// this method will check if it is a damagable object, if it is, make that object take the public amount of damage then instantiate the "dead enemy prefab"

	void OnHitObject(Collider col, Vector3 hitPoint) {
		
		IDamagable damagableObject = col.GetComponent<Collider>().GetComponent<IDamagable> ();

		if (damagableObject != null) {
			damagableObject.TakeHit (damage, hitPoint, transform.forward);
			Instantiate(alienModel, col.transform.position, col.transform.rotation);
		}
	}

	//If this gameobject collides with a player call the onhitobject method
	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag != "Player") {
			OnHitObject (collision.collider, Vector3.zero);

		}
	}
	}

