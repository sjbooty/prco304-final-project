﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAIInstantiateBullet : MonoBehaviour {
	public Transform turret;
	public Transform target;
	public GameObject projectile;
	public float speed = 5.0f;
	public float maxRange = 5.0f;
	public float shootTimer = 100.0f;
	private float counter = 0.0f;
	public AudioClip[] laserSounds;
	private AudioSource audio;
	RaycastHit hit;
	Rigidbody rigidBody;
	Vector3 SpawnVector;

	//Find relative components that are attached to this gameobject and fid the player in the scene, this is required as this script will be instantiated so will lose and editor set variables
	void Awake() {
		target = GameObject.FindWithTag ("Player").transform;
		audio = GetComponent<AudioSource> ();
		rigidBody = GetComponent<Rigidbody> ();
	}

	//This update method servers as an enemys ability to shoot an requires a large amount of cases to be correct before it will instantiate a bullet, 
	//these include: a time period, being able to see the player and being within a distance of the player,
	void Update () {
		counter++;
		if (target != null) {
			if (counter >= shootTimer) {
				if ((Vector3.Distance (transform.position, target.position) < maxRange)) {
					if (Physics.Raycast (transform.position, (target.position - transform.position), out hit, maxRange)) {
						if (Time.timeScale > 0.1f) {
					
							//SpawnVector = new Vector3 (turret.position.x, turret.position.y, turret.position.z);
							Vector3 relativePos = target.position - turret.position;
							Quaternion rotation = Quaternion.LookRotation (relativePos);
							GameObject instantiatedProjectile = Instantiate (projectile, turret.position, rotation) as GameObject;
							instantiatedProjectile.GetComponent<Rigidbody> ().velocity = turret.forward * speed;
							audio.clip = laserSounds [Random.Range (0, laserSounds.Length)];
							audio.Play ();
							counter = 0;
						}
					}
				}
			}
		}
	}
}
