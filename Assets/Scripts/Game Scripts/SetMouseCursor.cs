﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMouseCursor : MonoBehaviour {
	public Texture2D customMouseTexture;
	Vector2 customWidthHeight;
	public CursorMode cursorMode = CursorMode.Auto;


	void Start () {
			customWidthHeight = new Vector2 (customMouseTexture.width / 2, customMouseTexture.height / 2);
			Cursor.SetCursor (customMouseTexture, customWidthHeight, cursorMode);
	}

}
