﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomSound : MonoBehaviour {

	public AudioClip[] soundsToPlay;
	private AudioSource audio;

	void Start() {
		audio = GetComponent<AudioSource> ();
		audio.clip = soundsToPlay [Random.Range (0, soundsToPlay.Length)];  //Take a random sound from an array
		audio.Play ();  //play it
	}

}
