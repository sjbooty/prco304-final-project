﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour {
	public static int counter = 0;

//Ensure this object inst destroyed on scene change
	void Awake () {
		if (counter < 1) {
			DontDestroyOnLoad (this);
			counter++;
		} else {
			Destroy(gameObject);
		}
	}
}
