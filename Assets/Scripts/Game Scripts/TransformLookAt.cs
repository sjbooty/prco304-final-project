﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLookAt : MonoBehaviour {
	public Transform target;

	void Start() {
			target = GameObject.FindWithTag ("Player").transform;
	}
		//Look at the player in the scene
	void Update () {
		if (target != null) {
			transform.LookAt (target);
		}
	}
}
