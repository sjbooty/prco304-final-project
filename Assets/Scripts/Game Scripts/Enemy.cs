﻿using System.Collections;
using UnityEngine;


public class Enemy : Entity {
	Material shipMaterial;
	Color origionalColour;
	public GameObject deathEffect;
	public delegate void DeathAction();
	public static event DeathAction OnEnemyDeath;

	protected override void Start () {
		base.Start ();
	}

	//If the objects life total minus the damage taken is less than zero destroy the gameobject and instantiate an explosion prefab, also feeds the hit direction
	//so that the death effect can be mirrored from the direction to create a blood splatter effect
	public override void TakeHit (float damage, Vector3 hitPoint, Vector3 hitDirection)
	{
		if (damage >= health) {
			Destroy (Instantiate (deathEffect, hitPoint, Quaternion.FromToRotation (Vector3.forward, hitDirection)) as GameObject, 2);
			if (OnEnemyDeath != null) {
				OnEnemyDeath ();
			}
		}


		base.TakeHit (damage, hitPoint, hitDirection);
	}

	public void SetCharacteristics(Color colour) {
		shipMaterial = gameObject.GetComponentInChildren<Renderer> ().material;
		shipMaterial.color = colour;
		origionalColour = shipMaterial.color;
	}
}
