﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollisionDestroyInstantiate : MonoBehaviour {
	public GameObject explosionPrefab;

	//If an object collides with this object while it is active, spawn a prefab
	void OnCollisionStay(Collision collision) {
		if (collision.gameObject.tag == "Floor" || collision.gameObject.layer == LayerMask.NameToLayer("Unwalkable")) {
			Instantiate (explosionPrefab, transform.position, transform.rotation);
			GameObject.Destroy (gameObject);
		}
	}
}
