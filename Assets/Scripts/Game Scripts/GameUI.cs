﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour {

	public Image fadePlane;
	public GameObject gameOverUI;
	public RectTransform waveBanner;
	public Text levelTitle;
	public Text enemyCount;

	Spawner spawner;
	void Start () {
		FindObjectOfType<Player> ().OnDeath += OnGameOver;
	}

	void Awake() {
		spawner = FindObjectOfType<Spawner> ();
		spawner.OnNewWave += OnNewWave;
	}

	//Change the level title in the canvas to increment through the list of level names instead of numbers and move the banner up then down 
	void OnNewWave(int waveNumber) {
		string[] numbers = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten" };
		levelTitle.text = "Wave " + numbers[waveNumber-1];
		string enemyCountString = ((spawner.waves [waveNumber - 1].infinite) ? "Infinite" : spawner.waves [waveNumber - 1].enemyCount + "");
		enemyCount.text = "Enemies : " + enemyCountString;

		StartCoroutine (AnimateNewLevelBanner());
	}

	void OnGameOver() {
		//StartCoroutine(Fade (Color.clear, Color.black, 2));
		Debug.Log ("Working");
		gameOverUI.SetActive (true);
	}

	IEnumerator AnimateNewLevelBanner() {
		float speed = 1.5f;
		float delayTime = 2.0f;
		float animationPercent = 0;
		int dir = 1;

		float endDelayTime = Time.time + 1/speed + delayTime;
		while (animationPercent >= 0) {
			animationPercent += Time.deltaTime * speed * dir;

			if (animationPercent >= 1) {
				animationPercent = 1;
				if (Time.time > endDelayTime) {
					dir = -1;
				}
			}

			waveBanner.anchoredPosition = Vector2.up * Mathf.Lerp(-200, 18, animationPercent);
			yield return null;
		}
	}

	IEnumerator Fade(Color from, Color to, float time) {
		float speed = 1 / time;
		float percent = 0;

		while (percent < 1) {
			percent += Time.deltaTime * speed;
			fadePlane.color = Color.Lerp (from, to, percent);
			yield return null;
		}
	}

	//Used when the player has died and the scen will be reloaded
	public void StartNewGame() {
		Time.timeScale = 1.0f;
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}

}
