﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedRelativeToSound : MonoBehaviour {

	public CharacterController controller;
	public AudioSource audio;

	void Update() {
		audio.volume = (controller.velocity.magnitude / 25.0f / 3.0f) + 0.2f; //Plays audio that has its volume relative to the transofms velocity
	}
}
