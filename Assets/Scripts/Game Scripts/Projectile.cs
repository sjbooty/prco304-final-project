﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public LayerMask collisionMask;
	public float damage = 1.0f;
	float speed = 18;
	public float lifetime = 3.0f;
	float skinWidth = 0.1f;
	public AudioClip[] laserSounds;
	public AudioSource audio;
	public GameObject explosionPrefab;
	public GameObject explosionPrefabEnemyDeath;
	Rigidbody rb;
	public bool isRocket = false;
	public GameObject rocketPrefab;

	void Awake() {
		audio = GetComponent<AudioSource> ();
		rb = GetComponent<Rigidbody> ();
		Destroy (gameObject, lifetime);  //Destroy the obejct after its lifetime
		Collider[] initialCollisions = Physics.OverlapSphere (transform.position, 0.1f, collisionMask);
		if (initialCollisions.Length > 0) {
			OnHitObject (initialCollisions[0], transform.position);  //Check for initla collisions of bullets spawned
		}
	}

	void Update() {
		float moveDistance = speed * Time.deltaTime;
		CheckCollisions (moveDistance);
	}

	void CheckCollisions(float moveDistance) {  //Draw a ray in front of the bullet, if it collieds use the idamagable interface to allocate damage
		Ray ray = new Ray (transform.position, transform.forward);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, moveDistance + skinWidth, collisionMask, QueryTriggerInteraction.Collide)) {  //Cheks the collison mask for correct layers
			if (!isRocket) {
				OnHitObject (hit.collider, hit.point);
			} else {
				Instantiate (rocketPrefab, transform.position, transform.rotation);  //Instantiate a rocketexplosion if it is a rocket
			}
		}
	}
		
	void OnHitObject(Collider col, Vector3 hitPoint) {
		Debug.Log ("Hit");
		IDamagable damagableObject = col.GetComponent<Collider>().GetComponent<IDamagable> ();
		if (damagableObject != null) {
			damagableObject.TakeHit (damage, hitPoint, transform.forward);
		}
		Instantiate (explosionPrefabEnemyDeath, transform.position, transform.rotation);  //If an enemy is hit spawn their death prefab
		GameObject.Destroy (gameObject);
	}

	void OnCollisionEnter(Collision collision) {
		rb.isKinematic = true;
		if (!isRocket) {
			Instantiate (explosionPrefab, transform.position, transform.rotation);
		} else {
			Instantiate (rocketPrefab, transform.position, transform.rotation);
		}
		GameObject.Destroy (gameObject);
	}
}
