﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour, IDamagable{

	//This is the superclass to which all damagable object inherit from, inherited values are the entities health and wheter they are dead.

	public float startingHealth;
	public float health;
	protected bool dead;
	public bool destroyQuery = true;
	private ScoreManager scoreManager;

	public event System.Action OnDeath; //Unity's built in messaging system works well for triggering methods when no values are required

	protected virtual void Start() {
		health = startingHealth;
		scoreManager = Object.FindObjectOfType<ScoreManager> ();
	}

	public virtual void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection) {
		TakeDamage (damage);
	}

	//if health is less than or equal to zero trogger the die method
	public virtual void TakeDamage(float damage) {
		health -= damage;
		if (health <= 0 && !dead) {
			Die ();
		}
	}

	//If the onDeath message event exists trigger it
	protected void Die() {
		dead = true;
		if (OnDeath != null) {
			OnDeath();
		}

		//Check if this object should be destroyed when its health is below zero
		if (destroyQuery) {
			GameObject.Destroy (gameObject);
		} else {
			scoreManager.PlayerDied ();
			Time.timeScale = 0.0f;
		}
}

}
