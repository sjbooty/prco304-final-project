﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour {

	public Wave[] waves;   //Array of the total waves
	public Enemy enemy;		//Enemy variable to spawn (Should be array really? and randomised)
	public Transform player; //The player target
	public GameObject countdownPrefab; //Countdown timer prefab
	public ScoreManager scoreManager; //Calls the enemy death in score manager.

	Wave currentWave;  
	int currentWaveNumber;

	int enemiesLeftToSpawn = 0;
	int enemiesAlive;
	float timeForNExtSpawn = 0.0f;
	float counter = 1.0f;

	MapGeneration map;
	public GridBase gridHolder;

	public event System.Action <int> OnNewWave;

	void Awake() {
		player = GameObject.FindWithTag ("Player").transform;
		map = FindObjectOfType<MapGeneration> ();
		NextWave ();
	}

	void Update() {
		counter++;
		if (counter > 3.0f) {  //Spawn an enemy if there are enemies left to spawn
			if ((enemiesLeftToSpawn > 0 || currentWave.infinite) && Time.time > timeForNExtSpawn) {
				enemiesLeftToSpawn--;
				timeForNExtSpawn = Time.time + currentWave.timeBetweenSpawns;
				if (player != null) {
					StartCoroutine (SpawnEnemy ());
				}
			}
		}
	}

	IEnumerator SpawnEnemy() {  //Find a random tile to spawn the enemy, spawn the enemy and notify the action message
		float spawnDelay = 1;

		Transform randomTile = map.GetRandomOpenTile ();
		float spawnTimer = 0;

		while (spawnTimer < spawnDelay) {
			spawnTimer += Time.deltaTime;
			yield return null;
		}

		Enemy spawnedEnemy = Instantiate (enemy, randomTile.position + Vector3.up * 10.95f, Quaternion.identity) as Enemy;
		spawnedEnemy.OnDeath += OnEnemyDeath;
	}

	IEnumerator TimerBefore() {  //Move to the next wave, reset all variables related to enemies and start the countdown and wait before next level.
		yield return new WaitForSeconds(0.0f);

		currentWaveNumber++;
		if (currentWaveNumber - 1 < waves.Length) {
			currentWave = waves [currentWaveNumber - 1];



			if (OnNewWave != null) {
				if (currentWaveNumber > 1) {
					Instantiate (countdownPrefab);
					yield return new WaitForSeconds (4.0f);
				}
				OnNewWave (currentWaveNumber);
				gridHolder.CreateGrid ();
			}
			enemiesLeftToSpawn = currentWave.enemyCount;
			enemiesAlive = enemiesLeftToSpawn;
			ResetPlayerPosition ();
		}
	}

	void OnEnemyDeath() {
		enemiesAlive--;
		scoreManager.EnemyDied ();
		if (enemiesAlive == 0) {
			NextWave ();
		}
	}

	void ResetPlayerPosition() {
		player.position = new Vector3 (4.75f, 10.95f, 5.12f);
	}

	void NextWave() {
		StartCoroutine (TimerBefore ());
	}

	[System.Serializable]
	public class Wave {
		public bool infinite;
		public int enemyCount;
		public float timeBetweenSpawns;

	}
}
