﻿using UnityEngine;
using System.Collections;

public class ThirdPersonCamera : MonoBehaviour {


	[SerializeField]
	private float distanceAway;
	[SerializeField]
	private float distanceUp;
	[SerializeField]
	private float smooth;
	[SerializeField]
	public Transform follow;
	private Vector3 targetPosition;

	public static int health = 10;

	//private float distance = 10;




	void LateUpdate(){


		//set the target position to be on the correct offset from the object
		//targetPosition = follow.position + Vector3.up * distanceUp - follow.forward * distanceAway;

		if (follow != null) {
			Debug.DrawRay (follow.position, Vector3.up * distanceUp, Color.red);
			Debug.DrawRay (follow.position, -1f * follow.forward * distanceAway, Color.blue);
			Debug.DrawRay (follow.position, targetPosition, Color.magenta);

			if (Input.GetAxis ("Mouse ScrollWheel") > 0.0f && distanceUp > 10) {
				distanceUp--;
			} else if (Input.GetAxis ("Mouse ScrollWheel") < 0.0f && distanceUp <= 30) {
				distanceUp++;
			}
			//make a smooth transition between the current position an the position it should be in
			transform.position = new Vector3 (follow.position.x, follow.position.y + distanceUp, follow.position.z - distanceAway); //Follows from a stationary position
			//make camera look in correct direction
			transform.LookAt (follow);
		}

	}
}
