﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSeconds : MonoBehaviour {
	public float time = 0;

	//Destroy this gameobject after an amount of time
	void Start () {
		Destroy (gameObject, time);
	}

}
