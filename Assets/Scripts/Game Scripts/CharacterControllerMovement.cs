﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerMovement : MonoBehaviour {
	public Transform character;
	private float moveFB;
	private float moveLR;
	public float moveSpeed =  2.0f;


	//Takes in the vertical and horizontal values from the axis and converts them to a vector3 direction. The character controller is then
	//moved in the direction of the vector multiplied by the public movement speed in the editor
	void Update () { 
		moveFB = Input.GetAxis ("Vertical") * moveSpeed;   
		moveLR = Input.GetAxis ("Horizontal") * moveSpeed; 

		Vector3 movement = new Vector3 (moveLR, 0, moveFB); 

		character.GetComponent<CharacterController> ().Move (movement * Time.deltaTime); 

		Vector3 currentCorrectedPosition = new Vector3 (transform.position.x, 10.95f, transform.position.z);  //Corrects the transforms y position

		character.position = currentCorrectedPosition;
	}
}
