﻿using System.Collections;
using System.Collections.Generic;

public static class Utility {

	public static T[] ShuffleArray<T>(T[] array, int seed) {  //Takes and array of type T and a seed to randomise the order of the array, seed can be used to find seeds found previously.
		System.Random rndNumberSeed = new System.Random (seed);

		for (int i = 0; i < array.Length - 1; i++) {
			int randomIndex = rndNumberSeed.Next (i, array.Length);
			T tempItem = array [randomIndex];
			array [randomIndex] = array [i];
			array [i] = tempItem;
		}

		return array;
	}
}
