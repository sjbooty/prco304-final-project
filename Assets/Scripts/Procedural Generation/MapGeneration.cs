﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

public class MapGeneration : MonoBehaviour {


	public Map[] mapList;   //All of the map items
	public int mapNumber = 0;  //Current displayed map
	public Transform floorObject;  //Floor object prefab
	public Transform mapUnderfloor;  //Prefab from under floor to give depth
	public Transform[] buildingPrefabs; //List of buildings prefabs

	[Range(0,1)]
	public float floorTileSzieOutline;  //Size of the outline (actually size of tile itself that giveseffect of outline) 

	public float floorTileSize;  //Size of scale for floor tiles
	public float mapScale = 1.0f;  //Sclae of all map objects
	public bool randomLevels = false; //Bool to determine whether random seeds are needed

	List<TileCoord> totalTileCoordinates; //List of all tiles
	Queue<TileCoord> totalTileCoordinatesShuffled; //List of all tiles shuffled
	Queue<TileCoord> totalOpenCoordinatesShuffled; //List of all remainging remaing open tiles
	Transform[,] mapSize;

	Map thisMapNumber;  //Current map

	void Awake() {
		FindObjectOfType<Spawner> ().OnNewWave += OnNewWave;
	}

	void OnNewWave(int waveNumber) {  //When a new wave starts in the spawner, increase wave number (from spawner) and generate the next map
		mapNumber = waveNumber - 1;
		GenerateMap ();
	}

	public void GenerateMap() {  //Generate the map 
		thisMapNumber = mapList [mapNumber];
		mapSize = new Transform[thisMapNumber.mapSize.x, thisMapNumber.mapSize.y];  //Create map transform

		System.Random randomNumber = new System.Random (thisMapNumber.seed);
		totalTileCoordinates = new List<TileCoord>();
		for (int x = 0; x < thisMapNumber.mapSize.x; x++) {   //Loop through all tiles and add their coordinates to a new tile
			for (int y = 0; y < thisMapNumber.mapSize.y; y++) {
				totalTileCoordinates.Add (new TileCoord (x, y));
			}
		}  //If random tiles bool is selected, randomise seed for shuffledArray
		if(randomLevels) {
				totalTileCoordinatesShuffled = new Queue<TileCoord>(Utility.ShuffleArray(totalTileCoordinates.ToArray(), Random.Range (0, 1000000)));
		} else {
			totalTileCoordinatesShuffled = new Queue<TileCoord>(Utility.ShuffleArray(totalTileCoordinates.ToArray(), thisMapNumber.seed));
		}

		string mapObjectNameInScene = "MapGen";  //Destroy previous map so that no overlap is created
		if (transform.FindChild (mapObjectNameInScene)) {
			DestroyImmediate (transform.FindChild (mapObjectNameInScene).gameObject);
		}

		Transform mapObjectTransformInScene = new GameObject (mapObjectNameInScene).transform;
		mapObjectTransformInScene.parent = transform;

		for (int x = 0; x < thisMapNumber.mapSize.x; x++) {  //Add all of the floor panels to the scene with for loop, apply its scale and parent
			for (int y = 0; y < thisMapNumber.mapSize.y; y++) {
				Vector3 tileXY = TileCoordToPosition (x, y);
				Transform tileToAdd = Instantiate (floorObject, tileXY, Quaternion.Euler (Vector3.right * 90)) as Transform;
				tileToAdd.localScale = Vector3.one * (1 - floorTileSzieOutline) * floorTileSize * mapScale;
				tileToAdd.parent = mapObjectTransformInScene;
				mapSize [x, y] = tileToAdd;
			}
		}

		bool[,] mapCheckBuildings = new bool[(int)thisMapNumber.mapSize.x, (int)thisMapNumber.mapSize.y];

		int buildingNumber = (int)(thisMapNumber.mapSize.x * thisMapNumber.mapSize.y * thisMapNumber.obstaclePercent);
		int currentBuildingTotal = 0;
		List<TileCoord> allOpenPositions = new List<TileCoord> (totalTileCoordinates);

		for (int i = 0; i < buildingNumber; i++) {  //Loop through number of buildings required, check that the position is not the center of map and that the map is open, create a lerp for high between two floats defined, select a random building to be placed from array
			TileCoord randomPosition = GetRandomCoord ();
			mapCheckBuildings [randomPosition.x, randomPosition.y] = true;
			currentBuildingTotal++;
			if (randomPosition != thisMapNumber.mapCenter && MapIsOpenFromCenterTile (mapCheckBuildings, currentBuildingTotal)) {
				float buildingSizeY = Mathf.Lerp (thisMapNumber.minObstacleHeight, thisMapNumber.maxObstacleHeight, (float)randomNumber.NextDouble ());
				Vector3 buildingPosition = TileCoordToPosition (randomPosition.x, randomPosition.y);

				int rndNum = Random.Range (0, buildingPrefabs.Length);
				int rndNumRot = Random.Range (0, 5);
				Transform buildingToAdd = Instantiate (buildingPrefabs[rndNum], buildingPosition, Quaternion.Euler(0, rndNumRot * 90, 0)) as Transform;  //Add building to random tile coordinate
				buildingToAdd.parent = mapObjectTransformInScene;
				buildingToAdd.localScale = new Vector3 (buildingToAdd.localScale.x, buildingToAdd.localScale.y * buildingSizeY/2, buildingToAdd.localScale.z); //Apply the universal scale


				Renderer buildingRenderer = buildingToAdd.GetComponentInChildren<Renderer> ();  //Find the building material and change its colour to lerp between two colours as they are created
				Material buildingMaterial = new Material (buildingRenderer.sharedMaterial);
				float colourLerp = randomPosition.y / (float)thisMapNumber.mapSize.y;
				buildingMaterial.color = Color.Lerp (thisMapNumber.foregroundColour, thisMapNumber.backgroundColour, colourLerp);
				buildingRenderer.sharedMaterial = buildingMaterial;
				allOpenPositions.Remove (randomPosition);
			
			} else {
				mapCheckBuildings [randomPosition.x, randomPosition.y] = false;
				currentBuildingTotal--;
			}
		}

		totalOpenCoordinatesShuffled = new Queue<TileCoord>(Utility.ShuffleArray(allOpenPositions.ToArray(), thisMapNumber.seed));
		mapUnderfloor.localScale = new Vector3 (thisMapNumber.mapSize.x * floorTileSize * mapScale * 2, thisMapNumber.mapSize.y * floorTileSize * mapScale * 2);
	}



	bool MapIsOpenFromCenterTile(bool[,] obstacleMap, int currentObstacleCount) {
		bool[,] mapCheck = new bool[obstacleMap.GetLength(0), obstacleMap.GetLength(1)];
		Queue<TileCoord> queue = new Queue<TileCoord>();
		queue.Enqueue(thisMapNumber.mapCenter);
		mapCheck[thisMapNumber.mapCenter.x,thisMapNumber.mapCenter.y] = true;

			int totalFindableTiles = 1;

			while(queue.Count > 0) {
				TileCoord coordinateTile = queue.Dequeue();

				for(int x = -1; x <= 1; x++) {
					for(int y = -1; y <= 1; y++) {
						int adjacentXTiles = coordinateTile.x + x;
						int adjacentYTiles = coordinateTile.y + y;
						if (x==0 || y==0) {
							if(adjacentXTiles >= 0 && adjacentXTiles < obstacleMap.GetLength(0) && adjacentYTiles >= 0 && adjacentYTiles < obstacleMap.GetLength(1)) {
								if(!mapCheck[adjacentXTiles,adjacentYTiles] && !obstacleMap[adjacentXTiles, adjacentYTiles]) {
									mapCheck[adjacentXTiles,adjacentYTiles] = true;
									queue.Enqueue(new TileCoord(adjacentXTiles,adjacentYTiles));
									totalFindableTiles ++;
								}
							}
						} 
					}
				}
			}

			int playerPositiveTiles = (int)(thisMapNumber.mapSize.x * thisMapNumber.mapSize.y - currentObstacleCount);
			return playerPositiveTiles == totalFindableTiles;
	}

	Vector3 TileCoordToPosition(int x, int y) {
		return new Vector3 (-thisMapNumber.mapSize.x / 2 + 0.5f + x, 0, -thisMapNumber.mapSize.y / 2 + 0.5f + y) * floorTileSize * mapScale;
	}

	public Transform FindTileFromWorldPoint(Vector3 position) {  //Take a real world point and find its relative tile
		int xPosition = Mathf.RoundToInt(position.x / floorTileSize + (thisMapNumber.mapSize.x - 1) / 2f);
		int yPosition = Mathf.RoundToInt(position.z / floorTileSize + (thisMapNumber.mapSize.y - 1) / 2f);
		xPosition = Mathf.Clamp (xPosition, 0, mapSize.GetLength (0) - 1);
		yPosition = Mathf.Clamp (yPosition, 0, mapSize.GetLength (1) - 1);
		return mapSize [xPosition, yPosition];
	}


	public TileCoord GetRandomCoord() {  //Find a random coordinate  
		TileCoord randomCoord = totalTileCoordinatesShuffled.Dequeue ();
		totalTileCoordinatesShuffled.Enqueue (randomCoord);
		return randomCoord;
	}

	public Transform GetRandomOpenTile() {  //Find a random tile that is still open
		TileCoord randomCoord = totalOpenCoordinatesShuffled.Dequeue ();
		totalOpenCoordinatesShuffled.Enqueue (randomCoord);
		return mapSize [randomCoord.x, randomCoord.y];
	}

	[System.Serializable]
	public struct TileCoord { //Tiles hold their x and y coordinates
		public int x;
		public int y;

		public TileCoord(int _x, int _y) {
			x = _x;
			y = _y;
		}

		public static bool operator == (TileCoord c1, TileCoord c2) { //Check for equal coordinates
			return c1.x == c2.x && c1.y == c2.y;
		}

		public static bool operator != (TileCoord c1, TileCoord c2) { //Check if two coordinates are not equal
			return !(c1 == c2);
		}
	}

	[System.Serializable]
	public class Map {  //Each map object hold the following variables and can be edited in the inspector
		public TileCoord mapSize;
		[Range(0,1)]
		public float obstaclePercent;
		public int seed;
		public float minObstacleHeight;
		public float maxObstacleHeight;
		public Color foregroundColour;
		public Color backgroundColour;

		public TileCoord mapCenter {
			get {
				return new TileCoord (mapSize.x / 2, mapSize.y / 2);
			}
		}
	}

}
