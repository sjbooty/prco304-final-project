﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof (MapGeneration))]
public class MapEditor : Editor {

	public override void OnInspectorGUI () {
		
		MapGeneration map = target as MapGeneration;

		if (DrawDefaultInspector ()) {
			map.GenerateMap ();
		}
			
		if (GUILayout.Button ("Generate New Map")) {
			map.GenerateMap ();
		}
	}
}
